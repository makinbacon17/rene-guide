# Setting Up Build Environment
```bash
# Grab platform tools & add to path
export BUILDBASE=[path-to-build-root]
wget https://dl.google.com/android/repository/platform-tools-latest-linux.zip
unzip platform-tools-latest-linux.zip -d ~
cat >> ~/.profile<< EOF
# add Android SDK platform tools to path
if [ -d "\$HOME/platform-tools" ] ; then
    PATH="\$HOME/platform-tools:\$PATH"
fi
EOF

# Update and grab new packages
sudo apt update
sudo apt upgrade
sudo apt install bc bison build-essential ccache curl flex g++-multilib gcc-multilib git gnupg gperf imagemagick lib32ncurses5-dev lib32readline-dev \ 
lib32z1-dev liblz4-tool libncurses5 libncurses5-dev libsdl1.2-dev libssl-dev libxml2 libxml2-utils lzop pngcrush rsync schedtool squashfs-tools \
xsltproc zip zlib1g-dev

# Packages formatted for those on Ubuntu:
sudo apt install bc bison build-essential ccache curl flex g++-multilib gcc-multilib git gnupg gperf imagemagick lib32ncurses5-dev lib32readline-dev lib32z1-dev liblz4-tool libncurses5 libncurses5-dev libsdl1.2-dev libssl-dev libxml2 libxml2-utils lzop pngcrush rsync schedtool squashfs-tools xsltproc zip zlib1g-dev


# Only needed for Ubuntu < 20.04:
sudo apt install libwxgtk3.0-dev

# Only needed for Ubuntu < 16.04:
sudo apt install libwxgtk2.8-dev

# Create folders
mkdir -p ~/bin
mkdir -p $BUILDBASE/android/lineage

# Grab repo command
curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
chmod a+x ~/bin/repo

# Only needed for older versions of Ubuntu:
cat >> ~/.profile<< EOF
# set PATH so it includes user's private bin if it exists
if [ -d "\$HOME/bin" ] ; then
    PATH="\$HOME/bin:\$PATH"
fi
EOF

# The below must be run on all version of Ubuntu, regardless of whether bin is added to path.
source ~/.profile

# Configure Git
git config --global user.email "you@example.com"
git config --global user.name "Your Name"
```
